package org.khmeracademy.v3.forum.controller;

import java.util.ArrayList;

import org.khmeracademy.v3.forum.entities.Comment;
import org.khmeracademy.v3.forum.services.commentService.commentServicesImpl.CommentServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("/comment")
public class CommentController {
@Autowired CommentServicesImpl im;
	
	@RequestMapping(value="/getAllComment",method=RequestMethod.GET)
	@ResponseBody
	ArrayList<Comment> getAllComment(){
		return im.getAllComment();
	}
	
	@RequestMapping(value="/getComment/{id}",method=RequestMethod.GET)
	@ResponseBody
	Comment getComment(@PathVariable("id") int id){
		return im.getComment(id);
	}
	
	@RequestMapping(value="/createComment",method=RequestMethod.POST)
	@ResponseBody
	boolean createComment(@ModelAttribute Comment comment){
		return im.createComment(comment);
	}
	@RequestMapping(value="/updateComment",method=RequestMethod.PUT)
	@ResponseBody
	boolean updateComment(@ModelAttribute Comment comment){
		return im.updateComment(comment);
	}
	
	@RequestMapping(value="/deleteCommentRecord/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	boolean deleteCommentRecord(@PathVariable("id") int id){
		return im.deleteCommentRecord(id);
	}
	
	@RequestMapping(value="/removeComment/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	boolean removeComment(@PathVariable("id") int id){
		return im.removeComment(id);
	}
}
