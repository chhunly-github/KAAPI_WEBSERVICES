
	package org.khmeracademy.v3.forum.controller;

	import java.util.ArrayList;


import org.khmeracademy.v3.forum.entities.GroupCover;
import org.khmeracademy.v3.forum.services.groupServices.groupServicesImpl.GroupCoverServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.ResponseBody;

	@Controller()
	public class GroupCoverController {
		@Qualifier("GroupCoverServicesImpl")
		@Autowired GroupCoverServicesImpl im;

		@RequestMapping(value="/getAllGroupCover",method=RequestMethod.GET)
		@ResponseBody
		ArrayList<GroupCover> getAllGroupCover(){
			return im.getAllGroupCover();
		}
		
		@RequestMapping(value="/getGroupGroupCover/{id}",method=RequestMethod.GET)
		@ResponseBody
		GroupCover getGroupCover(@PathVariable("id") int id){
			return im.getGroupCover(id);
		}
		
		@RequestMapping(value="/createGroupCover",method=RequestMethod.POST)
		@ResponseBody
		boolean createGroupCover(@ModelAttribute GroupCover Group){
			return im.createGroupCover(Group);
		}
		@RequestMapping(value="/updateGroupCover",method=RequestMethod.PUT)
		@ResponseBody
		boolean updateGroupCover(@ModelAttribute GroupCover Group){
			return im.updateGroupCover(Group);
		}
		
		@RequestMapping(value="/removeGroupCover/{id}",method=RequestMethod.POST)
		@ResponseBody
		public boolean removeGroupCover(@PathVariable("group_cover_id") int group_cover_id){
			return im.removeGroupCover(group_cover_id);
		}
		
		@RequestMapping(value="/deleteGroupCoverRow/{id}",method=RequestMethod.DELETE)
		@ResponseBody
		public boolean deleteGroupCoverRow(@PathVariable("group_cover_id")  int group_cover_id){
			return im.deleteGroupCoverRow(group_cover_id);
		}
	}

