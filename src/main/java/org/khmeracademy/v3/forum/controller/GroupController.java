package org.khmeracademy.v3.forum.controller;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.khmeracademy.v3.forum.entities.Group;
import org.khmeracademy.v3.forum.services.groupServices.groupServicesImpl.GroupServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller()
public class GroupController {
	@Qualifier("GroupServicesImpl")
	@Autowired GroupServicesImpl im;
	
	@RequestMapping(value="/getAllGroup",method=RequestMethod.GET)
	@ResponseBody
	ArrayList<Group> getAllGroup(){
		return im.getAllGroupCover();
	}
	
	@RequestMapping(value="/getGroup/{id}",method=RequestMethod.GET)
	@ResponseBody
	Group getGroup(@PathVariable("id") int id){
		return im.getGroup(id);
	}
	
	@RequestMapping(value="/createGroup",method=RequestMethod.POST)
	@ResponseBody
	boolean createGroup(@ModelAttribute Group Group){
		return im.createGroup(Group);
	}
	@RequestMapping(value="/updateGroup",method=RequestMethod.PUT)
	@ResponseBody
	boolean updateGroup(@ModelAttribute Group Group){
		return im.updateGroup(Group);
	}
	
	@RequestMapping(value="/removeGroup/{id}",method=RequestMethod.POST)
	@ResponseBody
	public boolean removeGroup(@Param("group_id") int group_id){
		return im.removeGroup(group_id);
	} 
	
	@RequestMapping(value="/removeGroup/{id}",method=RequestMethod.DELETE)
	@ResponseBody
	public boolean deleteRowGroup(@Param("group_id") int group_id){
		return im.deleteRowGroup(group_id);
	}
}
