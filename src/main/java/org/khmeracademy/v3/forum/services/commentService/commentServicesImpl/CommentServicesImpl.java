package org.khmeracademy.v3.forum.services.commentService.commentServicesImpl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.khmeracademy.v3.forum.entities.Comment;
import org.khmeracademy.v3.forum.entities.User;
import org.khmeracademy.v3.forum.repositories.commentRepository.CommentRepository;
import org.khmeracademy.v3.forum.services.commentService.CommentService;

@Service
public class CommentServicesImpl implements CommentService{

	@Autowired CommentRepository com;
	@Override
	public ArrayList<Comment> getAllComment() {
		// TODO Auto-generated method stub
		return com.getAllComment();
	}
	
	
	public boolean deleteCommentRecord(int comment_id){
		return com.deleteCommentRecord(comment_id);
	}
	
	@Override
	public Comment getComment(int comment_id) {
		return com.getComment(comment_id);
	}

	@Override
	public boolean createComment(Comment user) {
		return com.createComment(user);
	}

	@Override
	public boolean updateComment(Comment comment) {
		// TODO Auto-generated method stub
		return com.updateComment(comment);
	}

	@Override
	public boolean removeComment(int comment_id) {
		return com.removeComment(comment_id);
	}

	
}
