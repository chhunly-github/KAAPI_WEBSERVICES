package org.khmeracademy.v3.forum.services.groupServices;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.khmeracademy.v3.forum.entities.GroupCover;

public interface GroupCoverServices {

	public ArrayList<GroupCover> getAllGroupCover();
	public GroupCover getGroupCover(int group_cover_id);
	public boolean createGroupCover(GroupCover group_cover);
	public boolean updateGroupCover(GroupCover group_cover);
	public boolean removeGroupCover(int group_cover_id);
	public boolean deleteGroupCoverRow( int group_cover_id);

}	
