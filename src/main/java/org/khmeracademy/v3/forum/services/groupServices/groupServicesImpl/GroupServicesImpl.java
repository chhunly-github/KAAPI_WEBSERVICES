package org.khmeracademy.v3.forum.services.groupServices.groupServicesImpl;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.khmeracademy.v3.forum.entities.Group;
import org.khmeracademy.v3.forum.repositories.groupRepository.GroupRepository;
import org.khmeracademy.v3.forum.services.groupServices.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
@Service("GroupServicesImpl")
public class GroupServicesImpl implements GroupService {
	@Autowired GroupRepository gr;
		
	@Override
	public ArrayList<Group> getAllGroupCover() {
		return gr.getAllGroup();
	}

	@Override
	public Group getGroup(int group_id) {
		return gr.getGroup(group_id);
	}

	@Override
	public boolean createGroup(Group group) {
		return gr.createGroup(group);
	}

	@Override
	public boolean updateGroup(Group group) {
		return gr.updateGroup(group);
	}

	@Override
	public boolean removeGroup(@Param("group_id") int group_id){
		return gr.removeGroup(group_id);
	} 
	@Override
	public boolean deleteRowGroup(@Param("group_id") int group_id){
		return gr.deleteRowGroup(group_id);
	}
}
