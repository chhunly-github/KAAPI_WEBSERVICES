package org.khmeracademy.v3.forum.services.groupServices.groupServicesImpl;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.khmeracademy.v3.forum.entities.GroupCover;
import org.khmeracademy.v3.forum.repositories.groupRepository.GroupCoverRepository;
import org.khmeracademy.v3.forum.services.groupServices.GroupCoverServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
@Service("GroupCoverServicesImpl")
public class GroupCoverServicesImpl implements GroupCoverServices {
	@Qualifier("GroupCoverRepository")
	@Autowired  GroupCoverRepository gcr;

	@Override
	public ArrayList<GroupCover> getAllGroupCover() {
		return gcr.getAllGroupCover();
	}

	@Override
	public GroupCover getGroupCover(int group_cover_id) {
		
		return gcr.getGroupCover(group_cover_id);
	}

	@Override
	public boolean createGroupCover(GroupCover group_cover) {
		
		return gcr.createGroupCover(group_cover);
	}

	@Override
	public boolean updateGroupCover(GroupCover group_cover) {
		
		return gcr.updateGroupCover(group_cover);
	}

	@Override
	public boolean removeGroupCover(int group_cover_id) {
		
		return gcr.removeGroupCover(group_cover_id);
	}
	@Override
	public boolean deleteGroupCoverRow(int group_cover_id){
		return gcr.deleteGroupCoverRow(group_cover_id);
	}

}
