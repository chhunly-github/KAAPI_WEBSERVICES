package org.khmeracademy.v3.forum.services.commentService;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.khmeracademy.v3.forum.entities.Comment;
import org.khmeracademy.v3.forum.entities.User;

public interface CommentService {
	
	public ArrayList<Comment> getAllComment();
	public Comment getComment(int comment_id);
	public boolean createComment(Comment comment);
	public boolean updateComment(Comment comment);
	public boolean deleteCommentRecord( int comment_id);
	public boolean removeComment(int comment_id);
	
}
