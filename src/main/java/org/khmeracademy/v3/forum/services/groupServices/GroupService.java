package org.khmeracademy.v3.forum.services.groupServices;

import java.security.acl.Group;
import java.util.ArrayList;
public interface GroupService {
	public ArrayList<Group> getAllGroupCover();
	public Group getGroup(int group_id);
	public boolean createGroup(Group group);
	public boolean updateGroup(Group group);
	public boolean removeGroup(int group_id);
	public boolean deleteRowGroup(int group_id);
}
