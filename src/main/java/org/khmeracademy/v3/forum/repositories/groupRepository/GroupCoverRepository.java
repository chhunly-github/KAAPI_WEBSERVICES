package org.khmeracademy.v3.forum.repositories.groupRepository;

import java.util.ArrayList;

import org.apache.ibatis.annotations.*;
import org.khmeracademy.v3.forum.entities.GroupCover;
import org.springframework.stereotype.Repository;
@Repository("GroupCoverRepository")
public interface GroupCoverRepository {

		String C_Group="INSERT INTO ka_group_cover(group_cover_id,group_id,group_cover_images,group_cover_date_create,group_cover_active)"
						+ " VALUES(#{group_cover_id},#{group_id},#{group_cover_images},#{group_cover_date_create},#{group_cover_active})";
		
		String rs_Group ="SELECT * FROM ka_group_cover";
		
		String R_Group ="SELECT * FROM ka_group_cover WHERE group_cover_id=#{group_cover_id}";
		
		String U_Group="UPDATE ka_group_cover SET "
						+ "group_cover_images=#{group_cover_images},group_cover_date_create=#{group_cover_date_create},"
						+ "group_cover_active=#{group_cover_active},"
						+ " WHERE group_cover_id=#{group_cover_id}";
		
		String remove_Group="UPDATE ka_group_cover SET "
						+ "group_cover_active=0"
						+ " WHERE group_cover_id=#{group_cover_id}";
		
		String D_Group ="DELETE FROM ka_group_cover WHERE group_cover_id=#{group_cover_id}";
		
		
		@Select(rs_Group)
		public ArrayList<GroupCover> getAllGroupCover();
		
		@Select(R_Group)
		public GroupCover getGroupCover(@Param("group_cover_id") int group_cover_id);
		
		@Insert(C_Group)
		public boolean createGroupCover(GroupCover group_cover);
		
		@Update(U_Group)
		public boolean updateGroupCover(GroupCover group_cover);
		
		@Update(remove_Group)
		public boolean removeGroupCover(@Param("group_cover_id") int group_cover_id);
		
		@Delete(D_Group)
		public boolean deleteGroupCoverRow(@Param("group_cover_id") int group_cover_id);
}
