package org.khmeracademy.v3.forum.repositories.groupRepository;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.khmeracademy.v3.forum.entities.Group;

public interface GroupRepository {

	String C_Group="INSERT INTO ka_group(group_id,group_name,group_desc,group_date_create,group_active)"
					+ " VALUES(#{group_id},#{group_name},#{group_desc},#{group_date_create},#{group_active})";
	
	String rs_Group ="SELECT * FROM ka_group";
	
	String R_Group ="SELECT * FROM ka_group WHERE group_id=#{group_id}";
	
	String U_Group="UPDATE ka_group SET "
					+ "group_name=#{group_name},group_desc=#{group_desc},group_date_create=#{group_date_create},"
					+ "group_active=#{group_active}"
					+ " WHERE group_id=#{group_id}";
	
	String removeGroup="UPDATE ka_group SET "
					+ "group_active= 0"
					+ " WHERE group_id=#{group_id}";
	
	String D_Group ="DELETE FROM ka_group WHERE group_id=#{group_id}";
	
	
	@Select(rs_Group)
	public ArrayList<Group> getAllGroup();
	
	@Select(R_Group)
	public Group getGroup(@Param("group_id") int group_id);
	
	@Insert(C_Group)
	public boolean createGroup(Group group);
	
	@Update(U_Group)
	public boolean updateGroup(Group group);
	
	@Update(removeGroup)
	public boolean removeGroup(@Param("group_id") int group_id);
	
	@Delete(D_Group)
	public boolean deleteRowGroup(@Param("group_id") int group_id);
}
