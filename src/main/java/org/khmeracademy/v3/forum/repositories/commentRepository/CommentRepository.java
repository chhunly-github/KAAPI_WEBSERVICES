package org.khmeracademy.v3.forum.repositories.commentRepository;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import org.khmeracademy.v3.forum.entities.Comment;
import org.khmeracademy.v3.forum.entities.User;

public interface CommentRepository {

	/*
	 * C_MAINCATEGORY create
	 *  read 
	 * U_MAINCATEGORY update
	 * D_MAINCATEGORY delete
	 */
	
	String C_comment="INSERT INTO ka_comment(comment_id,comments,post_id,user_id,parent_id,comment_date_create,comment_active)"
					+ " VALUES(#{comment_id},#{comments},#{post_id},#{user_id},#{parent_id}"
					+ ",#{comment_date_create},#{comment_active})";
	
	String rs_comment ="SELECT * FROM ka_comment";
	
	String R_comment ="SELECT * FROM ka_comment WHERE comment_id=#{comment_id}";
	
	String U_comment="UPDATE ka_comment SET "
					+ "comments=#{comments},post_id=#{post_id},user_id=#{user_id},"
					+ "parent_id=#{parent_id},comment_date_create=#{comment_date_create},comment_active=#{comment_active}"
					+ " WHERE comment_id=#{comment_id}";
	
	String Remove_comment="UPDATE ka_comment SET comment_active=0"
			+ " WHERE comment_id=#{comment_id}";
	
	String deleteCommentRecord ="DELETE FROM ka_comment WHERE comment_id=#{comment_id}";
	
	
	@Select(rs_comment)
	public ArrayList<Comment> getAllComment();
	
	@Select(R_comment)
	public Comment getComment(@Param("comment_id") int comment_id);
	
	@Insert(C_comment)
	public boolean createComment(Comment comment);
	
	@Update(U_comment)
	public boolean updateComment(Comment comment);
	
	@Delete(deleteCommentRecord)
	public boolean deleteCommentRecord(@Param("comment_id") int comment_id);
	
	@Update(Remove_comment)
	public boolean removeComment(@Param("comment_id") int comment_id);
}
