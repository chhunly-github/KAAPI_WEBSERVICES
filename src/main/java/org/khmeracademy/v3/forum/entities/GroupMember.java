package org.khmeracademy.v3.forum.entities;

import java.io.Serializable;
import java.sql.Date;

public class GroupMember implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int group_member_id;
	private String group_member_role;
	private int user_id;
	private int group_id;
	private Date group_member_date_create;
	private int group_member_active;
	
	public GroupMember() {
		// TODO Auto-generated constructor stub
	}

	public GroupMember(int group_member_id, String group_member_role, int user_id, int group_id,
			Date group_member_date_create, int group_member_active) {
		super();
		this.group_member_id = group_member_id;
		this.group_member_role = group_member_role;
		this.user_id = user_id;
		this.group_id = group_id;
		this.group_member_date_create = group_member_date_create;
		this.group_member_active = group_member_active;
	}

	public int getGroup_member_id() {
		return group_member_id;
	}

	public void setGroup_member_id(int group_member_id) {
		this.group_member_id = group_member_id;
	}

	public String getGroup_member_role() {
		return group_member_role;
	}

	public void setGroup_member_role(String group_member_role) {
		this.group_member_role = group_member_role;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	public Date getGroup_member_date_create() {
		return group_member_date_create;
	}

	public void setGroup_member_date_create(Date group_member_date_create) {
		this.group_member_date_create = group_member_date_create;
	}

	public int getGroup_member_active() {
		return group_member_active;
	}

	public void setGroup_member_active(int group_member_active) {
		this.group_member_active = group_member_active;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
