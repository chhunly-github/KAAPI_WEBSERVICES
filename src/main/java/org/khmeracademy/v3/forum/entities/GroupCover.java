package org.khmeracademy.v3.forum.entities;

import java.io.Serializable;
import java.sql.Date;

public class GroupCover implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int group_cover_id;
	private int group_id;
	private String group_cover_images;
	private Date group_cover_date_create;
	private int group_cover_active;
	
	public GroupCover() {
		// TODO Auto-generated constructor stub
	}

	public GroupCover(int group_cover_id, int group_id, String group_cover_images, Date group_cover_date_create,
			int group_cover_active) {
		super();
		this.group_cover_id = group_cover_id;
		this.group_id = group_id;
		this.group_cover_images = group_cover_images;
		this.group_cover_date_create = group_cover_date_create;
		this.group_cover_active = group_cover_active;
	}

	public int getGroup_cover_id() {
		return group_cover_id;
	}

	public void setGroup_cover_id(int group_cover_id) {
		this.group_cover_id = group_cover_id;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	public String getGroup_cover_images() {
		return group_cover_images;
	}

	public void setGroup_cover_images(String group_cover_images) {
		this.group_cover_images = group_cover_images;
	}

	public Date getGroup_cover_date_create() {
		return group_cover_date_create;
	}

	public void setGroup_cover_date_create(Date group_cover_date_create) {
		this.group_cover_date_create = group_cover_date_create;
	}

	public int getGroup_cover_active() {
		return group_cover_active;
	}

	public void setGroup_cover_active(int group_cover_active) {
		this.group_cover_active = group_cover_active;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
