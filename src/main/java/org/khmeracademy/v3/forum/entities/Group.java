package org.khmeracademy.v3.forum.entities;

import java.io.Serializable;
import java.sql.Date;

public class Group implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int group_id;
	private int user_id;
	private String group_name;
	private String group_desc;
	private Date group_date_create;
	private int group_active;
	
	public Group() {
		// TODO Auto-generated constructor stub
	}
	
	public Group(int group_id, int user_id, String group_name, String group_desc, Date group_date_create,
			int group_active) {
		super();
		this.group_id = group_id;
		this.user_id = user_id;
		this.group_name = group_name;
		this.group_desc = group_desc;
		this.group_date_create = group_date_create;
		this.group_active = group_active;
	}
	
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getGroup_desc() {
		return group_desc;
	}
	public void setGroup_desc(String group_desc) {
		this.group_desc = group_desc;
	}
	public Date getGroup_date_create() {
		return group_date_create;
	}
	public void setGroup_date_create(Date group_date_create) {
		this.group_date_create = group_date_create;
	}
	public int getGroup_active() {
		return group_active;
	}
	public void setGroup_active(int group_active) {
		this.group_active = group_active;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
