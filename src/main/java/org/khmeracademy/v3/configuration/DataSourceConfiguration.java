package org.khmeracademy.v3.configuration;


import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@MapperScan("org.khmeracademy.v3.forum.repositories")
public class DataSourceConfiguration {
	@Autowired
	private DataSource datasource;
	

	@Bean 
	public DataSource getDataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5434/KA_KPS_DB");
		dataSource.setUsername("rina");
		dataSource.setPassword("rina");
		return dataSource;
	}
	
	@Bean
	public SqlSessionFactoryBean sqlsessionfactorybean(){
		SqlSessionFactoryBean sqlsessionfactoryBean = new SqlSessionFactoryBean();
		sqlsessionfactoryBean.setDataSource(datasource);
		return sqlsessionfactoryBean;
	}
	
	@Bean
	public DataSourceTransactionManager transactionManager(){
		return new DataSourceTransactionManager(datasource);
	}
}
